const Axios = require('axios')
const jsonParse = require('../../lib/jsonParse')
const Header = require('../../lib/getHeader')
const Dict = require('../../schemas/xiecheng').Dict;
const RecommendHotal = require('../../schemas/xiecheng').RecommendHotal;
const HotCitys = require('../../schemas/xiecheng').HotCitys;
const HotTravels = require('../../schemas/xiecheng').HotTravels;
const HotFlights = require('../../schemas/xiecheng').HotFlights;
const HotRecommends = require('../../schemas/xiecheng').HotRecommends;

module.exports = {
    /**
     * 获取字典
     */
    async getDict(req, res) {
        const body = await Header.getParams(req, res, ['dictValue'])
        Dict.findOne({
            dictValue: body.dictValue
        }).then(dict => {
            jsonParse.sendResult(res, 200, dict.dictList, '查询成功')
        })
    },
    /**
     * 获取海内外热门城市
     */
    async getCity(req, res) {
        HotCitys.find().then(citys => {
            const OverCity = citys.filter(e => e.oversea == "0")
            const ChCity = citys.filter(e => e.oversea == "1")
            const data = [{
                    label: "国内热门城市",
                    options: OverCity,
                },
                {
                    label: "海外热门城市",
                    options: ChCity,
                }
            ]

            jsonParse.sendResult(res, 200, data, '查询成功')
        })
    },
    /**
     * 获取热门推荐酒店
     * @param {cityId} req 
     * @param {*} res 
     */
    async getRecommendHotal(req, res) {
        const body = await Header.getParams(req, res, ['cityId'])
        let data = {
            cityId: body.cityId
        }
        RecommendHotal.find(data).then(hotal => {
            jsonParse.sendResult(res, 200, hotal, '获取成功')
        })
    },
    /**
     * 获取热门推荐旅游
     * @param {cityId} req 
     * @param {*} res 
     */
    async getHotTravels(req, res) {
        const data = {
            departureCities: {
                $elemMatch: {
                    cityId: req.body.cityId
                }
            },
        }
        HotTravels.find(data).limit(5).then(travels => {
            jsonParse.sendResult(res, 200, travels, '获取成功')
        })
    },
    /**
     * 获取热门推荐机票
     * @param {cityId} req 
     * @param {*} res 
     */
    async getHotFlights(req, res) {
        const data = {
            departCityId: req.body.cityId
        }
        HotFlights.find(data).then(travels => {
            jsonParse.sendResult(res, 200, travels, '获取成功')
        })
    },
    /**
     * 获取天气
     * @param {cityId} req 
     * @param {*} res 
     */
    async getWeatherInfo(cityCode) {
        return new Promise((resolve, reject) => {
            const cityCodes = cityCode
            Axios({
                url: `https://devapi.qweather.com/v7/weather/now?location=${cityCodes}&key=d1720ee4ac164938aa431f03e6e87bf5`,
                method: 'GET'
            }).then((body) => {
                resolve(body.data)
            })
        })
    },
    /**content-type
     * 获取热门推荐旅游
     * @param {cityId} req 
     * @param {*} res 
     */
    async getRecommendCityHotal(req, res) {
        HotRecommends.aggregate([{
            $lookup: {
                from: 'recommend_hotals',
                localField: 'cityId',
                foreignField: 'cityId',
                as: 'recommendCityHotal'
            }
        }]).then(async citys => {
            let list = []
            for (let o of citys) {
                const weatherInfo = await module.exports.getWeatherInfo(o.cityCode)
                list.push({
                    ...o,
                    recommendCityHotal: o.recommendCityHotal.splice(0, 3),
                    weatherInfo: weatherInfo.now
                })
            }
            jsonParse.sendResult(res, 200, list, '获取成功')
        })
    },

}