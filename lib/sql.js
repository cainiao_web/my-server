
// 查询
db.getCollection("shopCar").find()
// 多维数组查询
db.getCollection("hot_travels").find({departureCities: {$elemMatch: {'cityId' : 1}}})
// 点查询
db.getCollection("hot_travels").find({'departureCities.cityId': {$eq: 1}})

// 添加/更新字段  multi：是否全部
db.getCollection("shopCar").updateMany({}, {$set: {orderStatus:"0"}}, {multi: true})

// 删除字段 multi：是否全部
db.getCollection("shopCar").updateMany({}, {$unset: {goodsType:"", aaaa: ""}}, {multi: true})

// 删除
db.getCollection("shopCar").deleteMany({goodsCode:{$in:["PN0003"]}, userName: '123'})

// 新增
db.getCollection("dict_collect").save({dictValue:"houses_level", dictList: [
    {
      label: "不限",
      value: "",
    },
    {
      label: "二星（钻）及以下",
      value: "1",
    },
    {
      label: "三星（钻）",
      value: "2",
    },
    {
      label: "四星（钻）",
      value: "3",
    },
    {
      label: "五星（钻）",
      value: "4",
    },
  ]})

