const jwt = require('jsonwebtoken');
const jsonParse = require('./jsonParse')

const getUser = (req) => {
    const token = req.headers['authorization'].replace('Bearer ', '');
    const decodeResult = jwt.decode(token)
    return decodeResult.name
}

const getParams = (req, res, arr) => {
    const body = req.body
    const flag = arr.some(e => { return !body[e] })
    if(flag) {
        jsonParse.sendResult(res, '-2006')
        throw(Error('缺少参数'))
    } 
    return body
}

module.exports = {
    getUser,
    getParams
}