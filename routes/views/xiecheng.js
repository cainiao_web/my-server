module.exports = {
    '/getDict': {
        handler: 'getDict',
        needVerify: false
    },
    '/getRecommendHotal': {
        handler: 'getRecommendHotal',
        needVerify: false
    },
    '/getCity': {
        handler: 'getCity',
        needVerify: false
    },
    '/getHotTravels': {
        handler: 'getHotTravels',
        needVerify: false
    },
    '/getHotFlights': {
        handler: 'getHotFlights',
        needVerify: false
    },
    '/getRecommendCityHotal': {
        handler: 'getRecommendCityHotal',
        needVerify: false
    },
    '/getWeatherInfo': {
        handler: 'getWeatherInfo',
        needVerify: false
    }
}