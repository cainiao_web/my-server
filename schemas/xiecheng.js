const mongoose = require("mongoose");
const Schema = mongoose.Schema;
/**
 * 字典接口
 * dict_collect
 */
const dictSchema = new Schema({
  dictValue: String,
  dictList: Array
})

const Dict = mongoose.model("dict_collects", dictSchema);

/**
 * 酒店推荐
 * recommend_hotal
 */

const RecommendHotalSchema = new Schema({
  img: String,
  hotelName: String,
  price: String,
  deletePrice: String,
  starIcon: String,
  url: String,
  isAdHotel: String,
  adTraceId: String,
  scenario: String,
  comment: Object,
  cityId: String,
})
/**
 * 连接到数据库
 * 注意点：recommend_hotals为数据库表的名称，如果名称不带s，数据库的表名会自动带上
 * 解决方案：如果实在不需要s，可以在model中，加上第三个参数，表名。
 */
const RecommendHotal = mongoose.model("recommend_hotals", RecommendHotalSchema);

/**
 * 目的地
 * hot_citys
 */
const HotCitysSchema = new Schema({
  id: String,
  displayName: String,
  sort: String,
  timeOffset: String,
  oversea: String,
  type: String,
  countryId: String,
  provinceId: String,
})

const HotCitys = mongoose.model("hot_citys", HotCitysSchema);

/**
 * 当季热卖.跟团游
 * hot_citys
 */
const HotTravelSchema = new Schema({
  active: String,
  departureCities: Array,
  destinationCityId: String,
  destinationCityName: String,
  displayProductTypeName: String,
  imgUrl: String,
  mainName: String,
  maxTravelDays: String,
  minTravelDays: String,
  name: String,
  persons: String,
  pmRecommendations: Array,
  priproductCategoryIdce: String,
  productDiamondLevel: String,
  productId: String,
  productPatternId: String,
  productPatternName: String,
  productScore: String,
  productSource: String,
  recInfoList: Array,
  saleInfoList: Array,
  saleMode: String,
  scoreInfoList: Array,
  subName: String,
  tagGroups: Array,
  tagInfoList: Array,
  tags: Array,
  targetLink: Object,
  tradePrice: String,
})

const HotTravels = mongoose.model("hot_travels", HotTravelSchema);


/**
 * 周末畅游.特价机票
 * hot_citys
 */
 const HotFlightSchema = new Schema({
  imgUrl: String,
  mainName: String,
  productScore: String,
  price: String,
  persons: String,
})

const HotFlights = mongoose.model("hot_flights", HotFlightSchema);

/**
 * 热门推荐城市酒店
 * hot_recommend
 */
 const HotRecommendSchema = new Schema({
  imgUrl: String,
  mainName: String,
  productScore: String,
  price: String,
  persons: String,
})

const HotRecommends = mongoose.model("hot_recommends", HotRecommendSchema);

/**
 * 整体导出
 */
module.exports = {
  Dict,
  RecommendHotal,
  HotCitys,
  HotTravels,
  HotFlights,
  HotRecommends
};